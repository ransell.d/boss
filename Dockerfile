FROM python:3.10-slim-bullseye

RUN apt-get update && apt-get install \
    git \
    -yq --no-install-suggests --no-install-recommends --allow-downgrades --allow-remove-essential --allow-change-held-packages \
  && apt-get clean

RUN pip3 install --upgrade --no-cache-dir \
  pip \
  setuptools

RUN pip3 install --upgrade --no-cache-dir \
  twine \
  "numpy<=1.23.5" \
  pytest \
  coverage \
  sphinx-rtd-theme \
  numpydoc \
  matplotlib \
  GPy

CMD /bin/bash
