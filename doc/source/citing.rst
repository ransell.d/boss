Citing BOSS 
==============

.. code-block:: bib

    @article{boss,
      title={Bayesian inference of atomistic structure in functional materials},
      author={Todorovi{\'c}, Milica and Gutmann, Michael U and Corander, Jukka and Rinke, Patrick},
      journal={Npj computational materials},
      volume={5},
      number={1},
      pages={35},
      year={2019},
      publisher={Nature Publishing Group}
    }
