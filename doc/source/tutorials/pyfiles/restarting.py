# # Restarting

# When BOSS runs, a restart file is produced, by default named `boss.rst`,
# which allows an optimization to be restarted at a later point if the results
# were not satisfactory or the original run was somehow aborted. This tutorial
# demonstrates how to use BOSS restart capabilities directly in a python script.
# We consider restarting the 2D optimization described in the resuming tutorial.
# Note that we reserve the word *resume* for continuing an optimization when an
# Initialized BOMain object already exists, and the word *restart* for when we
# recreate a BOMain object from disk using restart files.

from boss.bo.bo_main import BOMain
from boss.pp.pp_main import PPMain


def func_2d(X):
    x1 = X[0, 0]
    x2 = X[0, 1]
    y = 0.01 * ((x1 ** 2 + x2 - 11) ** 2 + (x1 + x2 ** 2 - 7) ** 2 + 20 * (x1 + x2))
    return y

# To recreate a BOMain object we use the `BOMain.from_file()` factory method,
# when doing so we have the option to change any keywords. Since the run we
# are restarting from had 30 iterations, we increase the number to 50 to get
# a more accurate minimum prediction.

bo = BOMain.from_file("boss.rst", f=func_2d, iterpts=50)
res = bo.run()
pred_min = res.select('mu_glmin', -1)
x_pred_min = res.select('x_glmin', -1)
print("Predicted global min after restarting: {} at x = {}".format(pred_min, x_pred_min))
pp = PPMain(res, pp_models=True, pp_acq_funs=True)
pp.run()

# note: During the restart above we had to specify the user function
# to the `from_file` factory method. If this is omitted, BOSS will try to import the
# user function used in the previous run. This saves us the trouble of having
# to redefine the function, but can lead to unwanted side effects since any statements within
# the global scope of the previous BOSS python script will automatically run during the import process.
# This can be avoided by writing BOSS scripts where only function definitions appear in the global scope and
# any other code is placed under an if-name-main statement.
