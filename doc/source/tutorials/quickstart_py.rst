Quickstart
===============
Note: you can also download this tutorial as a :download:`python script <pyfiles/quickstart.py>` or a :download:`notebook <notebooks/quickstart.ipynb>`.

We illustrate the basic usage by minimizing the Forrester function
:math:`f(x) = (6x - 2)^2 \sin(12x - 4)`  on the interval :math:`0 \le x \le 1`

.. code-block:: python

    import numpy as np
    from boss.bo.bo_main import BOMain
    from boss.pp.pp_main import PPMain

The first step to running BOSS typically consists of defining an objective function and
the optimization bounds, where the latter should be specified as a hypercube. 
For the Forrester problem, we define the function and bounds as follows

.. code-block:: python

    def func(X):
        x = X[0, 0]
        return (6*x-2)**2 * np.sin(12*x - 4)

    bounds = np.array([[0., 1.]])

Note that BOSS expects the objective function to take a single 2D numpy array 
as argument and return a scalar value (this behaviour can be modified).
Next, we import ``BOMain``, which will be used to launch and configure the optimization.
When creating this object we can supply any number of BOSS :ref:`keywords<Keywords>`,
these are used to provide essential input information and modify BOSS's behavior.
In the following, only a minimal set of keywords are provided for brevity.

.. code-block:: python

    bo = BOMain(
        func, 
        bounds,    
        yrange=[-1, 1],
        kernel='rbf',
        initpts=5,
        iterpts=10
    )

We are now ready to start the optimization. Once finished, a ``BOResults`` object
that provides easy access to several resulting quantities is returned. In addition, 
a summary of the optimization and restart data is written to separate files, by default
named ``boss.out`` and ``boss.rst``, respectively.

.. code-block:: python

    res = bo.run()

The ``BOResults`` object gives easy access to the most important results from the run.
For instance, we can get the predicted global minimum from the last iteration:

.. code-block:: python

   print('Predicted global min: ', res.select('mu_glmin', -1)) 

A more detailed description of the ``BOResults`` object can be found in the corresponding Results tutorial.
BOSS also supports automatic post-processing, generating useful plots and data dumps in a directory 
called ``postprocessing``:

.. code-block:: python

    pp = PPMain(res, pp_models=True, pp_acq_funcs=True)
    pp.run()
