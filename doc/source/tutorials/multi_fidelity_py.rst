Multi-fidelity optimization
===========================
Note: you can also download this tutorial as a :download:`python script <pyfiles/multi_fidelity.py>` or a :download:`notebook <notebooks/multi_fidelity.ipynb>`.

This tutorial demonstrates how to run multi-fidelity optimization with BOSS. Multi-fidelity optimization involves evaluating the same function at different accuracy levels or fidelities, and using low-fidelity evaluations to guide the search for the input parameters that optimize the high-fidelity evaluations.

.. code-block:: python

    import numpy as np
    from boss.bo.bo_main import BOMain

We start with problem definition. In this tutorial, we will use the one-variable multi-fidelity task proposed by `Forrester et al. (2007) <https://eprints.soton.ac.uk/64698/>`_. The objective is to minimize the function :math:`f(x) = (6x - 2)^2 \sin(12x - 4)` in the interval :math:`0 \le x \le 1`. We imagine that :math:`f(x)` is expensive to compute and that it would be cheaper to compute approximate values :math:`\hat f(x) = A f(x) + B(x-0.5) + C`, where :math:`A`, :math:`B`, and :math:`C` are constants.

.. code-block:: python

    def f(x):
        return (6 * x - 2) ** 2 * np.sin(12 * x - 4)

    def f_low(x, A=0.5, B=10, C=-5):
        return A * f(x) + B * (x - 0.5) + C

    bounds = np.array([[0., 1.]])
    
Next we will create a ``BOMain`` instance that uses a multi-task model to capture the relationship between fidelity levels and a multi-task acquisition rule to determine the fidelity level for each evaluation. The functions that correspond to the fidelity levels  are collected in a list, starting with the function to be optimized.

.. code-block:: python

    func_list = [f, f_low]

Each user function is modeled as a task in the multi-task model, and optimization will utilize a multi-task acquisition rule that considers both the model information and the evaluation cost associated with each function. The costs are listed in the same order as the user functions, and here we assume that each ``f`` evaluation costs 1 unit and each ``f_low`` evaluation costs 0.1 units.

.. code-block:: python

    task_cost = [1, 0.1]

Finally we set a cost limit with the keyword ``maxcost``. When this keyword is used, optimization will stop when the next acquisition would make the cumulative evaluation cost exceed the cost limit. In this tutorial, we set ``maxcost`` to 10 and increase ``iterpts`` to 100. We run optimization with a multi-task acquisition rule that combines ELCB acquisitions with a cost-aware task selection heuristic. The acquisition rule is selected with the keyword ``acqfn_name``.

.. code-block:: python

    bo = BOMain(
        func_list,
        bounds,
        kernel="rbf",
        initpts=2,
        iterpts=100,
	task_cost=task_cost,
	maxcost=maxcost,
	acqfn_name="elcb_multi",
   )

Compared to the standard optimization setup introduced in the `quickstart tutorial <https://cest-group.gitlab.io/boss/tutorials/quickstart_py.html>`_, we replaced the single user function ``func`` with a user function list ``func_list`` and switched to a multi-task acquisition rule that considers the evaluation costs associated with each user function. We could also replace ``initpts`` with ``task_initpts`` to control the number of initialization points at task level. For example, using ``task_initpts=[2, 20]`` would initialize the multi-task model with 2 ``f`` evaluations and 20 ``f_low`` evaluations. When ``initpts=2`` is used, both tasks are initialized with 2 evaluations. Here we run optimization with this setup.

.. code-block:: python

    res = bo.run()
    print('Iteration count: ', res.num_iters)
    print('Predicted global min: ', res.select('mu_glmin', -1))

Multi-fidelity optimization is expected to increase the iteration count but reduce the optimization cost compared to standard optimization. This is because the low-fidelity evaluations used in multi-fidelity optimization are not as informative about the global minimum as high-fidelity evaluations, but can provide a better information to cost ratio. The task index used in each acquisition is concatenated to the input values, and can be used to check which user function was used in each evaluation.

.. code-block:: python

    print(res.select('X'))


