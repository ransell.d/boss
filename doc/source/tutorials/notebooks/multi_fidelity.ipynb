{
 "cells": [
  {
   "cell_type": "markdown",
   "id": "6b5a77e7",
   "metadata": {},
   "source": [
    "# Multi-fidelity optimisation"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "337e0ae6",
   "metadata": {},
   "source": [
    "This tutorial demonstrates how to run multi-fidelity optimization with BOSS. Multi-fidelity optimization involves evaluating the same function at different accuracy levels or fidelities, and using low-fidelity evaluations to guide the search for the input parameters that optimize the high-fidelity evaluations."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "961d49b1",
   "metadata": {
    "lines_to_next_cell": 1
   },
   "outputs": [],
   "source": [
    "import numpy as np\n",
    "from boss.bo.bo_main import BOMain"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "46d73825",
   "metadata": {},
   "source": [
    "We start with problem definition. In this tutorial, we will use the one-variable multi-fidelity task proposed by [Forrester et al (2007)](https://eprints.soton.ac.uk/64698/).\n",
    "The objective is to minimize the function $f(x) = (6x - 2)^2 sin(12x - 4)$ in the interval $[0, 1]$. We imagine that $f(x)$ is expensive to compute and that it would be cheaper to compute approximate values $\\hat f(x) = A f(x) + B (x-0.5) + C$, where $A$, $B$, and $C$ are constants."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "233a9290",
   "metadata": {
    "lines_to_next_cell": 1
   },
   "outputs": [],
   "source": [
    "def f(x):\n",
    "    return (6 * x - 2) ** 2 * np.sin(12 * x - 4)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "4662bf49",
   "metadata": {
    "lines_to_next_cell": 1
   },
   "outputs": [],
   "source": [
    "def f_low(x, A=0.5, B=10, C=-5):\n",
    "    return A * f(x) + B * (x - 0.5) + C"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "d298b2b7",
   "metadata": {},
   "outputs": [],
   "source": [
    "bounds = np.array([[0., 1.]])"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "b2b9c561",
   "metadata": {},
   "source": [
    "Next we will create a `BOMain` instance that uses a multi-task model to capture the relationship between fidelity levels and a multi-task acquisition rule to determine the fidelity level for each evaluation. The functions that correspond to the fidelity levels  are collected in a list, starting with the function to be optimized."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "2b3cf796",
   "metadata": {},
   "outputs": [],
   "source": [
    "func_list = [f, f_low]"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "c35316e1",
   "metadata": {},
   "source": [
    "Each user function is modeled as a task in the multi-task model, and optimization will utilize a multi-task acquisition rule that considers both the model information and the evaluation cost associated with each function. The costs are listed in the same order as the user functions, and here we assume that each `f` evaluation costs 1 unit and each `f_low` evaluation costs 0.1 units."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "d83c9f45",
   "metadata": {},
   "outputs": [],
   "source": [
    "task_cost = [1, 0.1]"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "26276b27",
   "metadata": {},
   "source": [
    "Finally we set a cost limit with the keyword `maxcost`. When this keyword is used, optimization will stop when the next acquisition would make the cumulative evaluation cost exceed the cost limit. In this tutorial, we set `maxcost` to 10 and increase `iterpts` to 100. We run optimization with a multi-task acquisition rule that combines ELCB acquisitions with a cost-aware task selection heuristic. The acquisition rule is selected with the keyword `acqfn_name`."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "879d72c2",
   "metadata": {},
   "outputs": [],
   "source": [
    "bo = BOMain(\n",
    "    func_list,\n",
    "    bounds,\n",
    "    kernel=\"rbf\",\n",
    "    initpts=2,\n",
    "    iterpts=100,\n",
    "    acqfn_name=\"elcb_multi\",\n",
    "    maxcost=10,\n",
    "    task_cost=task_cost,\n",
    ")"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "a1d9db17",
   "metadata": {},
   "source": [
    "Compared to the standard optimization setup introduced in the [quickstart tutorial](https://cest-group.gitlab.io/boss/tutorials/quickstart_py.html), we replaced the single user function `func` with a user function list `func_list` and switched to a multi-task acquisition rule that considers the evaluation costs associated with each user function. We could also replace `initpts` with `task_initpts` to control the number of initialization points at task level. For example, using `task_initpts=[2, 20]` would initialize the multi-task model with 2 `f` evaluations and 20 `f_low` evaluations. When `initpts=2` is used, both tasks are initialized with 2 evaluations. Here we run optimization with this setup."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "8410ef6f",
   "metadata": {},
   "outputs": [],
   "source": [
    "res = bo.run()\n",
    "print('Iteration count: ', res.num_iters)\n",
    "print('Predicted global min: ', res.select('mu_glmin', -1))"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "6d43fe96",
   "metadata": {},
   "source": [
    "Multi-fidelity optimization is expected to increase the iteration count but reduce the optimization cost compared to standard optimization. This is because the low-fidelity evaluations used in multi-fidelity optimization are not as informative about the global minimum as high-fidelity evaluations, but can provide a better information to cost ratio. The task index used in each acquisition is concatenated to the input values, and can be used to check which user function was used in each evaluation."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "ca502be8",
   "metadata": {},
   "outputs": [],
   "source": [
    "print(res.select('X'))"
   ]
  }
 ],
 "metadata": {
  "jupytext": {
   "cell_metadata_filter": "-all",
   "main_language": "python",
   "notebook_metadata_filter": "-all"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 5
}
