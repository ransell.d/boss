from boss.utils.testing import get_test_func
from boss.bo.bo_main import BOMain


f = get_test_func('forrester')

bo = BOMain(
    f,
    bounds=f.bounds,
    yrange=f.frange,
    seed=22,
    initpts=2,
    iterpts=3,
    outfile='test_results_from_file.out',
    rstfile='test_results_from_file.rst',
)

res = bo.run()

# with open('tmp', mode='w') as fp:
#     fp.write(str(res.data))
