"""
Unit tests for available acquisition classes:
- All methods for each class (call, evaluate and evaluate with gradient)
- Each method is tested with a 1D single input and a 2D array input

The following methods are tested for every acquisition class:

1) Calling the class at location x.
2) Evaluate the acquisition class at x (this is the same as calling).
3) Evaluate with gradient at x: This returns the evaluation and the
    gradient of the acquisition class at x.

Each method is called and compared to a hardcoded value (up to
a fixed tolerance), denoted as 'expected_value' or 'expected_grad'.
"""
import pytest
import numpy as np

from unittest.mock import Mock
from numpy.testing import assert_allclose

from boss.bo.acq.lcb import LCB
from boss.bo.acq.elcb import ELCB
from boss.bo.acq.explore import Explore
from boss.bo.acq.exploit import Exploit
from boss.bo.acq.ei import EI
from boss.bo.acq.mes import MaxValueEntropySearch

TOL = 1e-6


@pytest.fixture
def random_seed():
    np.random.seed(123)


@pytest.fixture
def models():
    """
    The Mock objects imitate our BO models (a BO model is required to
    call the acquisition function).

    Each method is separately tested with a 1D and a 2D model. The input
    to the 1D model is always a single value. For the 2D model, an array
    as input is tested.
    """
    model_1d = Mock()
    mean, variance = 1, 4
    model_1d.X.shape = (1, 1)
    model_1d.Y = np.array([1.0])
    model_1d.dim = 1
    model_1d.bounds = np.array([[-3.0, 3.0]])
    model_1d.predict.return_value = (mean, variance)
    model_1d.predict_mean_sd_grads.return_value = (
        mean, np.sqrt(variance), np.array([3, 4]), np.array([5, 6])
        )
    model_1d.predict_grads.return_value = (
            np.array([[[3], [4]]]),
            np.array([[5, 6]]),
        )

    model_2d = Mock()
    mean = np.array([1, 2, 3]).reshape(-1, 1)
    variance = np.array([4, 5, 6]).reshape(-1, 1)
    model_2d.X.shape = (3, 2)
    model_2d.Y = np.array([1.0, 2.0, 3.0])
    model_2d.dim = 2
    model_2d.bounds = np.array([[-3.0, 3.0], [-3.0, 3.0]])
    model_2d.predict.return_value = (mean, variance)
    model_2d.predict_mean_sd_grads.return_value = (
        mean, np.sqrt(variance), np.array([[3, 4], [4, 4], [4, 2]]),
        np.array([[3, 3], [2, 2], [3, 6]])
        )
    model_2d.predict_grads.return_value = (
            np.array([[[3], [4]], [[4], [4]], [[4], [2]]]),
            np.array([[3, 3], [2, 2], [3, 6]]),
        )

    return model_1d, model_2d


def test_lcb(models):
    model_1d, model_2d = models[0], models[1]

    # 1D Tests
    x = np.array([0])
    weight = np.array([1.0])
    lcb = LCB(weight=weight)   # Test with user-defined parameter
    lcb.model = model_1d

    value = lcb(x)
    expected_value = np.array([-1])
    assert_allclose(value, expected_value, atol=TOL)

    value = lcb.evaluate(x)
    assert_allclose(value, expected_value, atol=TOL)

    value, grad = lcb.evaluate_with_gradient(x)
    expected_grad = np.array([-2, -2])
    assert_allclose(value, expected_value, atol=TOL)
    assert_allclose(grad, expected_grad, atol=TOL)

    lcb.weight = np.array([2.0])  # Test with the standard parameter

    value = lcb(x)
    expected_value = np.array([-3])
    assert_allclose(value, expected_value, atol=TOL)

    value = lcb.evaluate(x)
    assert_allclose(value, expected_value, atol=TOL)

    value, grad = lcb.evaluate_with_gradient(x)
    expected_grad = np.array([-7, -8])
    assert_allclose(value, expected_value, atol=TOL)
    assert_allclose(grad, expected_grad, atol=TOL)

    # 2D Tests
    x = np.array([[0, 0], [1, 1], [2, 2]])
    weight = np.array([1.0])  # Test with user-defined parameter
    lcb = LCB(weight=weight)
    lcb.model = model_2d

    value = lcb(x)
    expected_value = np.array([-1, -0.23606798, 0.55051026]).reshape(-1, 1)
    assert_allclose(value, expected_value, atol=TOL)

    value = lcb.evaluate(x)
    assert_allclose(value, expected_value, atol=TOL)

    value, grad = lcb.evaluate_with_gradient(np.array([[0, 0]]))
    expected_grad = np.array([[0, 2, 1], [1, 2, -4]]).T
    assert_allclose(value, expected_value, atol=TOL)
    assert_allclose(grad, expected_grad, atol=TOL)

    lcb.weight = np.array([2.0])  # Test with the standard parameter

    value = lcb(x)
    expected_value = np.array([-3, -2.47213595, -1.89897949]).reshape(-1, 1)
    assert_allclose(value, expected_value, atol=TOL)

    value = lcb.evaluate(x)
    assert_allclose(value, expected_value, atol=TOL)

    value, grad = lcb.evaluate_with_gradient(np.array([[0, 0]]))
    expected_grad = np.array([[-3, 0, -2], [-2, 0, -10]]).T
    assert_allclose(value, expected_value, atol=TOL)
    assert_allclose(grad, expected_grad, atol=TOL)


def test_ei(models):
    model_1d, model_2d = models[0], models[1]

    # 1D Tests
    x = np.array([0])
    jitter = np.array([1.0])
    ei = EI(jitter=jitter)   # Test with user-defined parameter
    ei.model = model_1d

    value = ei(x)
    expected_value = np.array([-0.395593])
    assert_allclose(value, expected_value, atol=TOL)

    value = ei.evaluate(x)
    assert_allclose(value, expected_value, atol=TOL)

    value, grad = ei.evaluate_with_gradient(x)
    expected_grad = np.array([0.485531, 0.706052])
    assert_allclose(value, expected_value, atol=TOL)
    assert_allclose(grad, expected_grad, atol=TOL)

    ei = EI()    # Test with the standard parameter
    ei.model = model_1d

    value = ei(x)
    expected_value = np.array([-0.797885])
    assert_allclose(value, expected_value, atol=TOL)

    value = ei.evaluate(x)
    assert_allclose(value, expected_value, atol=TOL)

    value, grad = ei.evaluate_with_gradient(x)
    expected_grad = np.array([1.001322, 1.401587])
    assert_allclose(value, expected_value, atol=TOL)
    assert_allclose(grad, expected_grad, atol=TOL)

    # 2D Tests
    x = np.array([[0, 0], [1, 1], [2, 2]])
    jitter = np.array([1.0])  # Test with user-defined parameter
    ei = EI(jitter=jitter)
    ei.model = model_2d

    value = ei(x)
    expected_value = np.array(
        [[-0.395593], [-0.226874], [-0.130592]]).reshape(-1, 1)
    assert_allclose(value, expected_value, atol=TOL)

    value = ei.evaluate(x)
    assert_allclose(value, expected_value, atol=TOL)

    value, grad = ei.evaluate_with_gradient(np.array([[0, 0]]))
    expected_grad = np.array([[0.661564, 0.622593, 0.325943],
                              [0.970101, 0.622593, -0.010128]]).T
    assert_allclose(value, expected_value, atol=TOL)
    assert_allclose(grad, expected_grad, atol=TOL)

    ei = EI()    # Test with the standard parameter
    ei.model = model_2d
    value = ei(x)
    expected_value = np.array(
        [[-0.797885], [-0.479811], [-0.285982]]).reshape(-1, 1)
    assert_allclose(value, expected_value, atol=TOL)

    value = ei.evaluate(x)
    assert_allclose(value, expected_value, atol=TOL)

    value, grad = ei.evaluate_with_gradient(np.array([[0, 0]]))
    expected_grad = np.array([[1.200793, 1.148007, 0.653383],
                              [1.700793, 1.148007, 0.064117]]).T
    assert_allclose(value, expected_value, atol=TOL)
    assert_allclose(grad, expected_grad, atol=TOL)


def test_elcb(models):
    model_1d, model_2d = models[0], models[1]

    # 1D Tests
    x = np.array([0])
    elcb = ELCB()
    elcb.model = model_1d

    value = elcb(x)
    expected_value = np.array([[-2.483881]])
    assert_allclose(value, expected_value, atol=TOL)

    value = elcb.evaluate(x)
    assert_allclose(value, expected_value, atol=TOL)

    value, grad = elcb.evaluate_with_gradient(x)
    expected_grad = np.array([-5.709703, -6.451643])
    assert_allclose(value, expected_value, atol=TOL)
    assert_allclose(grad, expected_grad, atol=TOL)

    # 2D Tests
    x = np.array([[0, 0], [1, 1], [2, 2]])
    elcb = ELCB()
    elcb.model = model_2d

    value = elcb(x)
    expected_value = np.array(
        [[-3.856783], [-3.430048], [-2.94832]]).reshape(-1, 1)
    assert_allclose(value, expected_value, atol=TOL)

    value = elcb.evaluate(x)
    assert_allclose(value, expected_value, atol=TOL)

    value, grad = elcb.evaluate_with_gradient(np.array([[0, 0]]))
    expected_grad = np.array([[-4.285174, -0.856783, -3.285174],
                              [-3.285174, -0.856783, -12.570348]]).T
    assert_allclose(value, expected_value, atol=TOL)
    assert_allclose(grad, expected_grad, atol=TOL)


def test_explore(models):
    model_1d, model_2d = models[0], models[1]

    # 1D Tests
    x = np.array([0])
    explore = Explore()
    explore.model = model_1d

    value = explore(x)
    expected_value = np.array([-2])
    assert_allclose(value, expected_value, atol=TOL)

    value = explore.evaluate(x)
    assert_allclose(value, expected_value, atol=TOL)

    value, grad = explore.evaluate_with_gradient(x)
    expected_grad = np.array([-5, -6])
    assert_allclose(value, expected_value, atol=TOL)
    assert_allclose(grad, expected_grad, atol=TOL)

    # 2D Tests
    x = np.array([[0, 0], [1, 1], [2, 2]])
    explore = Explore()
    explore.model = model_2d

    value = explore(x)
    expected_value = np.array(
        [[-2.], [-2.236068], [-2.44949]]).reshape(-1, 1)
    assert_allclose(value, expected_value, atol=TOL)

    value = explore.evaluate(x)
    assert_allclose(value, expected_value, atol=TOL)

    value, grad = explore.evaluate_with_gradient(np.array([[0, 0]]))
    expected_grad = np.array([[-3, -2, -3], [-3, -2, -6]]).T
    assert_allclose(value, expected_value, atol=TOL)
    assert_allclose(grad, expected_grad, atol=TOL)


def test_exploit(models):
    model_1d, model_2d = models[0], models[1]

    # 1D Tests
    x = np.array([0])
    exploit = Exploit()
    exploit.model = model_1d

    value = exploit(x)
    expected_value = np.array([1])
    assert_allclose(value, expected_value, atol=TOL)

    value = exploit.evaluate(x)
    assert_allclose(value, expected_value, atol=TOL)

    value, grad = exploit.evaluate_with_gradient(x)
    expected_grad = np.array([3, 4])
    assert_allclose(value, expected_value, atol=TOL)
    assert_allclose(grad, expected_grad, atol=TOL)

    # 2D Tests
    x = np.array([[0, 0], [1, 1], [2, 2]])
    exploit = Exploit()
    exploit.model = model_2d

    value = exploit(x)
    expected_value = np.array([[1], [2], [3]]).reshape(-1, 1)
    assert_allclose(value, expected_value, atol=TOL)

    value = exploit.evaluate(x)
    assert_allclose(value, expected_value, atol=TOL)

    value, grad = exploit.evaluate_with_gradient(np.array([[0, 0]]))
    expected_grad = np.array([[3, 4, 4], [4, 4, 2]]).T
    assert_allclose(value, expected_value, atol=TOL)
    assert_allclose(grad, expected_grad, atol=TOL)


def test_mes(models, random_seed):
    model_1d, model_2d = models[0], models[1]

    # 1D Tests
    x = np.array([[0], [2]])
    mes = MaxValueEntropySearch(model_1d.bounds)
    model_1d.predict.return_value = (np.array([0, 1]).reshape(-1, 1),
                                     np.array([1, 2]).reshape(-1, 1))
    mes.model = model_1d

    value = mes(x)
    expected_value = np.array([0.964386, 0.220565])
    assert_allclose(value, expected_value, atol=TOL)

    value = mes.evaluate(x)
    assert_allclose(value, expected_value, atol=TOL)

    # 2D Tests
    x = np.array([[0, 0], [1, 1], [2, 2]])
    mes = MaxValueEntropySearch(model_2d.bounds)
    mes.model = model_2d
    mes.model.X = x

    value = mes(x)
    expected_value = np.array([0.405711, 0.156979, 0.044924])
    assert_allclose(value, expected_value, atol=TOL)

    value = mes.evaluate(x)
    assert_allclose(value, expected_value, atol=TOL)
