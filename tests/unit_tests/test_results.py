from pathlib import Path

import numpy as np
import pytest
from pytest import approx

from boss.bo.results import BatchTracker, BOResults, minimize_model
from boss.settings import Settings
from boss.utils.sparselist import SparseList


@pytest.fixture
def basic_batch_tracker():
    init_batches = 2
    tracker = BatchTracker(init_batches)
    tracker.update(3)
    tracker._ind_lims.extend([4, 8, 10, 11])
    return tracker


def test_get_batch_indices(basic_batch_tracker):
    tracker = basic_batch_tracker

    assert tracker.get_batch_indices(0) == slice(0, 4)
    assert tracker.get_batch_indices(1) == slice(4, 8)
    assert tracker.get_batch_indices(2) == slice(8, 10)
    assert tracker.get_batch_indices(3) == slice(10, 11)

    tracker.num_init_batches = 3
    assert tracker.get_batch_indices(0) == slice(0, 8)
    assert tracker.get_batch_indices(1) == slice(8, 10)


def test_get_multibatch_indices(basic_batch_tracker):
    tracker = basic_batch_tracker

    inds = tracker.get_multibatch_indices([0, 2])
    assert np.array_equal(inds, np.array([0, 1, 2, 3, 8, 9]))

    inds = tracker.get_multibatch_indices([2, 3])
    assert inds == slice(8, 11)

    inds = tracker.get_multibatch_indices(0)
    assert inds == slice(0, 4)


def test_iteration_labels(basic_batch_tracker):
    tracker = basic_batch_tracker
    true_labels = [0, 0, 0, 0, 1, 1, 1, 1, 2, 2, 3]
    assert np.array_equal(tracker.iteration_labels, true_labels)

    tracker.num_init_batches = 1
    true_labels = [0, 0, 0, 1, 2, 2, 2, 2, 3, 3, 4]
    assert np.array_equal(tracker.iteration_labels, true_labels)


def test_batch_sizes(basic_batch_tracker):
    tracker = basic_batch_tracker
    assert np.array_equal(tracker.batch_sizes, [4, 4, 2, 1])

    tracker.num_init_batches = 1
    assert np.array_equal(tracker.batch_sizes, [3, 1, 4, 2, 1])


def test_ensemble_sizes(basic_batch_tracker):
    tracker = basic_batch_tracker
    assert np.array_equal(tracker.ensemble_sizes, [4, 8, 10, 11])

    tracker.num_init_batches = 1
    assert np.array_equal(tracker.ensemble_sizes, [3, 4, 8, 10, 11])


def test_num_iters(basic_batch_tracker):
    tracker = basic_batch_tracker
    assert tracker.num_iters == 4

    tracker.num_init_batches = 1
    assert tracker.num_iters == 5


@pytest.fixture
def results_batch():
    res = BOResults(settings=None)
    res.add_defaults()

    # Initial points
    X = np.arange(0, 15).reshape((5, 3))
    Y = np.arange(0, 5).reshape((5, 1))
    res["X"] = X
    res["Y"] = Y

    res.batch_tracker._ind_lims = [0, 2, 3, 5]
    res.batch_tracker.num_init_batches = 3

    # Add iterations
    X = np.arange(15, 30).reshape((5, 3))
    Y = np.array([-5, -7, -1, -11, -6])[:, None]
    res["X"] = np.vstack((res["X"], X))
    res["Y"] = np.vstack((res["Y"], Y))
    res.batch_tracker._ind_lims.extend([7, 8, 10])
    return res


@pytest.fixture
def results_run():
    # results from 5 datapts with forrester function
    data = {
        "X": np.array([[0.0], [0.5], [0.90430652], [0.28274337], [0.36275172]]),
        "Y": np.array(
            [[3.02720998], [0.90929743], [6.3184254], [-0.05256117], [0.01077164]]
        ),
        "X_next": np.array([0.31049234]),
        "mu_glmin": SparseList(
            {
                0: 0.8958555722069506,
                1: 0.8516556553775598,
                2: -0.1770016103715628,
                3: -0.07534281750157934,
            }, default=np.nan
        ),
        "nu_glmin": SparseList(
            {
                0: 0.14062550766333085,
                1: 0.339950158358178,
                2: 0.011424688616994328,
                3: 3.3718544631077544e-06,
            }, default=np.nan
        ),
        "x_glmin": SparseList(
            {
                0: np.array([0.52998784]),
                1: np.array([0.46696348]),
                2: np.array([0.33733282]),
                3: np.array([0.30946351]),
            }
        ),
        "model_params": SparseList(
            {
                0: np.array([7.86945069, 0.22077895]),
                1: np.array([11.23573095, 0.18505793]),
                2: np.array([14.65544684, 0.32374433]),
                3: np.array([31.32408691, 0.42172712]),
            }
        ),
    }

    res = BOResults.__new__(BOResults)
    res.data = data
    res.extendable_names = ["mu_glmin", "nu_glmin", "x_glmin", "model_params"]
    res.settings = Settings({"bounds": [0.0, 1.0], "seed": 22})
    res.batch_tracker = BatchTracker(num_init_batches=2)
    res.batch_tracker._ind_lims = list(range(6))
    return res


def test_get(results_batch):
    res = results_batch
    assert np.array_equal(res.get("X"), res.data.get("X"))


def test_getitem(results_batch):
    res = results_batch
    assert np.array_equal(res["X"], res.data["X"])


def test_setitem(results_batch):
    res = results_batch
    X2 = res.data["X"] ** 2
    res["X"] = X2
    assert np.array_equal(res.data["X"], X2)


def test_select(results_batch, results_run):
    res1 = results_batch
    sel = res1.select("X", 0)
    assert np.array_equal(sel, np.arange(15).reshape((5, 3)))

    sel = res1.select("X", 2)
    assert np.array_equal(sel, np.array([[21, 22, 23]]))

    sel = res1.select("X", -1)
    assert np.array_equal(sel, np.array([[24, 25, 26], [27, 28, 29]]))

    sel = res1.select("Y", [2, 3])
    assert np.array_equal(sel, np.array([-1, -11, -6])[:, None])

    res2 = results_run

    sel = res2.select("x_glmin", [1, 3])
    assert sel == approx(np.array([res2.data["x_glmin"][1], res2.data["x_glmin"][3]]))

    sel1 = res2.select("mu_glmin", -1)
    sel2 = res2.select("mu_glmin", 3)
    assert sel1 == approx(res2["mu_glmin"][-1])
    assert sel2 == approx(res2["mu_glmin"][3])

    sel = res2.select("nu_glmin", 0)
    assert sel == approx(res2["nu_glmin"][0])


def test_get_best_acq(results_batch):
    res = results_batch
    x_best, y_best = res.get_best_acq(0)

    assert np.array_equal(x_best, [0, 1, 2])
    assert np.array_equal(y_best, [0])

    x_best, y_best = res.get_best_acq(2)
    assert np.array_equal(x_best, [18, 19, 20])
    assert np.array_equal(y_best, [-7])

    x_best, y_best = res.get_best_acq(-1)
    assert np.array_equal(x_best, [24, 25, 26])
    assert np.array_equal(y_best, [-11])


def test_get_next_acq(results_batch, results_run):
    res1 = results_batch
    x_next = res1.get_next_acq(0)
    assert np.array_equal(x_next, [[15, 16, 17], [18, 19, 20]])

    x_next = res1.get_next_acq(1)
    assert np.array_equal(x_next, [[21, 22, 23]])

    x_next = res1.get_next_acq(-2)
    assert np.array_equal(x_next, [[24, 25, 26], [27, 28, 29]])

    res2 = results_run
    x_next1 = res2.get_next_acq(-1)
    x_next2 = res2.get_next_acq(3)
    assert x_next1 == approx(res2["X_next"][-1]) == x_next2


def test_get_est_yrange(results_batch):
    res = results_batch
    y_range = res.get_est_yrange(-1)
    assert np.array_equal(y_range, [-11, 4])


def test_reconstruct_model(results_run):
    res = results_run
    model = res.reconstruct_model(2)
    assert model.get_unfixed_params() == approx(res.select("model_params", 2))

    model = res.reconstruct_model(-1)
    assert model.get_unfixed_params() == approx(res.select("model_params", 3))


def test_minimize_model(results_run):
    res = results_run
    model = res.reconstruct_model(itr=-1)
    x, mu, nu = minimize_model(
        model,
        bounds=res.settings["bounds"],
        optimtype=res.settings["optimtype"],
        kernel=res.settings["kernel"],
        min_dist_acqs=res.settings["min_dist_acqs"],
    )
    assert x == approx(res.select("x_glmin", -1), rel=1e-5)
    assert mu == approx(res.select("mu_glmin", -1), rel=1e-5)
    assert nu == approx(res.select("nu_glmin", -1), rel=1e-5)


def test_calc_missing_minima(results_run):
    res = results_run
    itr = 2
    mu_glmin = res["mu_glmin"][itr]
    res["x_glmin"][itr] = None
    res["mu_glmin"][itr] = np.nan
    res["nu_glmin"][itr] = np.nan
    res.calc_missing_minima()
    assert res["mu_glmin"][itr] == approx(mu_glmin, rel=1e-5)


def test_from_file(results_run):
    res1 = results_run
    dir_data = Path(__file__).parent.resolve() / "data"
    res2 = BOResults.from_file(
        dir_data / "test_results_from_file.rst", dir_data / "test_results_from_file.out"
    )

    assert res1["X"] == approx(res2["X"])
    assert res1["Y"] == approx(res2["Y"])

    for k in res1["x_glmin"].keys():
        assert res1["x_glmin"][k] == approx(res2["x_glmin"][k])
        assert res1["mu_glmin"][k] == approx(res2["mu_glmin"][k])
        assert res1["nu_glmin"][k] == approx(res2["nu_glmin"][k])

    for k in res1["model_params"].keys():
        assert res1["model_params"][k] == approx(res2["model_params"][k])

    assert res1["X_next"] == approx(res2["X_next"])
