import unittest

import numpy as np
import pytest
from scipy.stats import qmc

from boss.bo.initmanager import InitManager


def test_sobol():
    bounds = np.array([[5.0, 10.0], [2.0, 4.0]])
    initpts = 5
    im = InitManager(inittype="sobol", bounds=bounds, initpts=initpts)
    X = im.get_all()
    assert X.shape == (initpts, len(bounds))

    # sobol points from scipy for comparison
    sampler = qmc.Sobol(d=2, scramble=False)
    X_ref = sampler.random(initpts)
    X_ref = qmc.scale(X_ref, bounds[:, 0], bounds[:, 1])
    assert X == pytest.approx(X_ref)


def test_grid():
    # 2d example
    bounds = np.array([[5.0, 10.0], [1.0, 4.0]])
    initpts = 3
    im = InitManager(inittype="grid", bounds=bounds, initpts=initpts)
    X = im.get_all()
    assert X.shape != (initpts, len(bounds))
    X_ref = np.array([[5.0, 1.0], [5.0, 4.0], [10.0, 1.0], [10.0, 4.0]])
    assert X == pytest.approx(X_ref)

    # 3d example
    bounds = np.array([[5.0, 10.0], [1.0, 4.0], [11.0, 15.0]])
    initpts = 8
    im = InitManager(inittype="grid", bounds=bounds, initpts=initpts)
    X = im.get_all()
    assert X.shape == (initpts, len(bounds))
    X_ref = np.array(
        [
            [5.0, 1.0, 11.0],
            [5.0, 1.0, 15.0],
            [5.0, 4.0, 11.0],
            [5.0, 4.0, 15.0],
            [10.0, 1.0, 11.0],
            [10.0, 1.0, 15.0],
            [10.0, 4.0, 11.0],
            [10.0, 4.0, 15.0],
        ]
    )
    assert X == pytest.approx(X_ref)


def test_random():
    seed = 221209
    rng = np.random.default_rng(seed)
    bounds = np.array([[5.0, 10.0], [1.0, 4.0]])
    initpts = 4
    im = InitManager(inittype="random", bounds=bounds, initpts=initpts, seed=rng)
    X = im.get_all()

    rng = np.random.default_rng(seed)
    X_ref = rng.random((initpts, len(bounds)))
    X_ref = bounds[:, 0] + (bounds[:, 1] - bounds[:, 0]) * X_ref
    assert X == pytest.approx(X_ref)


def test_extend():
    bounds = np.array([[5.0, 10.0], [1.0, 4.0]])
    initpts = 4
    im = InitManager(inittype="sobol", bounds=bounds, initpts=initpts)
    X = im.get_all()
    n1 = 3
    n2 = 5
    with pytest.raises(ValueError):
        X_ext = im._extend(X, [n1, n2])

    n2 = 4
    X_ext = im._extend(X, [n1, n2])
    assert X[:n1, :] == pytest.approx(X_ext[:n1, :-1])
    assert X[:n2, :] == pytest.approx(X_ext[n1:, :-1])


def check_multi(inittype, initpts):
    seed = 221209
    bounds = np.array([[5.0, 10.0], [1.0, 4.0]])
    dim = len(bounds)
    test_points = InitManager(inittype, bounds, initpts, seed=seed).get_all()
    base_points = []
    for npts in initpts:
        points = InitManager(inittype, bounds, npts, seed=seed).get_all()
        base_points.append(points)
    base_points = np.vstack(base_points)
    assert test_points[:, :dim] == pytest.approx(base_points)


def test_multi():
    for inittype in ["sobol", "grid", "random"]:
        check_multi(inittype, [8])
        check_multi(inittype, [8, 5])
