from unittest.mock import Mock, MagicMock

import numpy as np
import pytest
from numpy.testing import assert_allclose

from boss.bo.acq.elcb import ELCB
from boss.bo.acq.cost import CostAwareAcquisition
from boss.bo.acq.explore import Explore
from boss.bo.acq.manager import Sequential
from boss.bo.acq.factory import select_acq_manager
from boss.bo.acq.manager import _is_explore


@pytest.fixture
def acqfn1d():
    model = Mock()
    mean = np.array([[1.0]])
    variance = np.array([[0.26]])
    model.dim = 2
    model.predict.return_value = (mean, variance)

    acqfn = Mock()
    acqfn.model = model
    acqfn.minimize.return_value = np.array([-1.5])
    return acqfn


def test_sequential(acqfn1d):
    bounds = np.array([[-3.0, 3.0]])
    acqman = Sequential(acqfn1d, bounds, optimtype="score", acqtol=0.5)
    assert isinstance(acqman.explorefn, Explore)

    # tesst is_loc_overconfident
    x_next = np.atleast_2d(acqfn1d.minimize(bounds))
    assert not acqman.is_loc_overconfident(x_next)
    acqman.acqtol = 0.52
    assert acqman.is_loc_overconfident(x_next)
    acqman.acqtol = None
    assert not acqman.is_loc_overconfident(x_next)

    # test acquire
    acqman.acqtol = 0.1
    assert_allclose(acqman.acquire(), x_next)


def test_select_acq_manager():
    data = {
        'batchtype': 'Sequential',
        'bounds': np.atleast_2d([0., 1.]),
        'optimtype': 'score',
        'acqtol': None,
        'acqfn': Explore(),
    }
    settings = MagicMock()
    settings.__getitem__.side_effect = data.__getitem__    
    settings.is_multi = False
    acq_manager = select_acq_manager(settings)
    assert isinstance(acq_manager, Sequential)
    assert_allclose(settings['bounds'], acq_manager.bounds)

    settings.is_multi = True
    acq_manager = select_acq_manager(settings)
    desired_bounds = np.vstack((settings['bounds'], [[0, 0]]))
    assert_allclose(acq_manager.bounds, desired_bounds)


def test_is_explore():
    acqfn = Mock()
    parent = Mock()

    acqfn.__class__ = Explore
    assert _is_explore(acqfn)

    acqfn.__class__ = ELCB
    assert not _is_explore(acqfn)

    acqfn.__class__ = CostAwareAcquisition
    parent.__class__ = Explore
    acqfn.acqfn = parent
    assert _is_explore(acqfn)

    parent.__class__ = ELCB
    assert not _is_explore(acqfn)
