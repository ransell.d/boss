import numpy as np
import pytest

from boss.bo.kernel_factory import KernelFactory
from boss.settings import Settings

from boss.bo.kernel_factory import KernelFactory


@pytest.fixture
def settings():
    sts = Settings(dict(bounds=np.array([[0.0, 1.0]] * 4)))
    dim = sts.dim
    sts["thetainit"] = [9.0, 2.1, 2.2, 2.3, 2.4]
    sts["thetaprior"] = "gamma"
    sts["thetapriorpar"] = [[1, 1]] * (dim + 1)
    sts["kernel"] = ["stdp", "rbf", "mat32", "mat52"]
    sts["thetabounds"] = [(0, 10)] * (dim + 1)
    sts["periods"] = [4.0, None, None, None]
    return sts


def test_default(settings):
    kern = KernelFactory.construct_kernel(settings)
    lengthscales = settings["thetainit"][1:]
    variances = [settings["thetainit"][0]] + [1.0] * (settings.dim - 1)
    for i, k in enumerate(kern.parts):
        assert k.lengthscale[0] == lengthscales[i]
        assert k.variance[0] == variances[i]


@pytest.fixture
def settings_multi():
    sts = Settings(dict(bounds=np.array([[0.0, 1.0]] * 4)))
    dim = sts.dim
    sts["thetainit"] = [9.0, 2.1, 2.2, 2.3, 2.4]
    sts["thetaprior"] = "gamma"
    sts["thetapriorpar"] = [[1, 1]] * (dim + 1)
    sts["kernel"] = ["stdp", "rbf", "mat32", "mat52"]
    sts["thetabounds"] = [(0, 10)] * (dim + 1)
    sts["periods"] = [4.0, None, None, None]
    sts["num_tasks"] = 2
    sts["W_rank"] = 1
    sts["W_init"] = 0.5*np.ones(2)
    sts["kappa_init"] = np.ones(2)
    return sts


def test_multi(settings_multi):
    kern = KernelFactory.construct_kernel(settings_multi)
    lengthscales = settings_multi["thetainit"][1:]
    for i, k in enumerate(kern.parts[:-1]):
        assert k.lengthscale[0] == lengthscales[i]
        assert k.variance[0] == 1.0
    assert np.all(np.squeeze(kern.B.W) == settings_multi['W_init'])
    assert np.all(np.squeeze(kern.B.kappa) == settings_multi['kappa_init'])
