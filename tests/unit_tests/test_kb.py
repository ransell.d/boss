from unittest.mock import MagicMock, Mock

import numpy as np
import pytest
from numpy.testing import assert_allclose

from boss.bo.acq.kb import KrigingBeliever


@pytest.fixture
def acqfn2d():
    model = Mock()
    mean = np.array([[1.0]])
    variance = np.array([[0.26]])
    model.dim = 2
    model.predict.return_value = (mean, variance)

    acqfn = Mock()
    acqfn.model = model
    acqfn.minimize.return_value = np.array([-1.5, 1.0])
    return acqfn


def test_kriging_believer(acqfn2d):
    bounds = np.array([[-3.0, 3.0], [-3.0, 3.0]])
    acqman = KrigingBeliever(acqfn2d, bounds, batchpts=2, optimtype="score")
    X_batch = acqman.acquire()
    assert_allclose(X_batch, np.array([[-1.5, 1.0], [-1.5, 1.0]]))
